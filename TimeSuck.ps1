﻿#Get latest time entry for user. If there is not a time entry for the last X hours set start time for start of shift.
#https://developer.connectwise.com/Manage/REST
$Version = 0.1

#Exit if already open
$AlreadyRunning = Get-WmiObject -Class Win32_Process -Filter "name='powershell.exe'" | Where-Object {$_.CommandLine -eq $((Get-WmiObject -Class Win32_Process -Filter "ProcessId='$PID'").CommandLine) -and $_.ProcessId -ne $pid}
if($AlreadyRunning -ne $null){
    Get-Date | Out-File "$(Split-Path -Parent $MyInvocation.MyCommand.Path)\AlreadyRunning.log"
    exit
}

$pubkey = ''
$privatekey = ''
$IntegratorUser = ''
$IntegratorPass = '='
$MemberID = ''
$CWServer = ''
$CWLoginCompany = ''
$CompanyDisplayName = ''
$ScheduledStart = ''
$ScheduledEnd = ''
$User = ''
$TimeFrame = 60
$AutoClose = $true
$TimeWrapChargeCode = ''
$TaskName = 'ConnectWiseTimeReminder'

$Quotes = 'All your time are belong to us!','Time keeps on slipping slipping, into the future.'

$ScriptName = $MyInvocation.MyCommand.Name
$ScriptPath = $MyInvocation.MyCommand.Path
$ScriptDir = Split-Path -Parent $ScriptPath
$sLogPath = $ScriptPath -replace '.ps1','.log'

 #Dot Source required Function Libraries
    (new-object Net.WebClient).DownloadString("https://raw.githubusercontent.com/ChrisTaylorRocks/Powershell-Logging/master/Powershell-Logging.ps1") | iex

function Get-CWAuthHeader{
    param(
        $CWLoginCompany,
        $pubkey,
        $privatekey
    )
    $Authstring  = $CWLoginCompany + '+' + $pubkey + ':' + $privatekey
    $encodedAuth  = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(($Authstring)));
    $Headers=@{
        Authorization = "Basic $encodedAuth"
    }
    return $Headers
}

function Convert-Time {
    param([Parameter(ValueFromPipeline = $true)]$DateTime)
    $SomeTime = "{0:yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'}" -f $DateTime.ToUniversalTime()
    Return $SomeTime    
}

function Test-Time{
    param(
        [Parameter(Mandatory=$true)]
        $Time
    )
    try{
        [datetime]$Validate = $Time
        return $Validate
    }
    catch{
        return $null
    }
}

function Get-CWImpersinateAuth {
    param(
        $MemberID,
        $IntegratorUser,
        $IntegratorPass
    )
    $URL = "https://$($CWServer)/v4_6_release/apis/3.0/system/members/$($MemberID)/tokens"
    
    $Authstring  = $CWLoginCompany + '+' + $IntegratorUser + ':' + $IntegratorPass
    $encodedAuth  = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(($Authstring)));
    $Headers = @{
        Authorization = "Basic $encodedAuth"
        'x-cw-usertype' ="integrator"
    }

    $Body = @{
        memberIdentifier = $User
    }
    
    $Result = Invoke-RestMethod -Method Post -Uri $URL -Headers $Headers -Body $Body

    $Authstring  = $CWLoginCompany + '+' + $Result.publicKey + ':' + $Result.privateKey
    $encodedAuth  = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(($Authstring)));
    $Headers=@{
        Authorization = "Basic $encodedAuth"
    }

    return $Headers
}

function Get-ChargeCode{
    param(
        $Headers
    )
    $APIData = Invoke-RestMethod -Headers $Headers -Uri "https://$($CWServer)/v4_6_release/apis/3.0/system/reports/ChargeCode" -Method GET
    $Item = @{}
    For ($a=0; $a -lt $APIData.row_values.count; $a++){
        For ($b=0; $b -lt $APIData.column_definitions.count; $b++){
            $Property += @{$(($APIData.column_definitions[$b] | Get-Member -MemberType NoteProperty).Name) = $($APIData.row_values[$a][$b])}
        }
        $Item.add($Property.Description,$Property)
        Remove-Variable Property -ErrorAction SilentlyContinue
    }
    return $Item
}

Start-Log -LogPath $sLogPath -ToScreen -ScriptVersion $Version

#Get auth token
$Headers = Get-CWAuthHeader -CWLoginCompany $CWLoginCompany -pubkey $pubkey -privatekey $privatekey

#if $TimeFrame set make sure ther is a scheduled task.
if($TimeFrame){
    #Setup scheduled task
    
    $TaskQuery = Get-ScheduledTask -TaskName $TaskName -ErrorAction SilentlyContinue
    if(!$TaskQuery){
        if(-not $(Test-Path "$ScriptDir\SilentLaunchPoSh.vbs")){
            Write-LogInfo -LogPath $sLogPath -ToScreen -TimeStamp -Message "Downloading SilentLaunchPoSh.vbs"
            $url = "https://gitlab.com/ChrisTaylorRocks/ConnectWise-TimeEntryReminder/raw/master/SilentLaunchPoSh.vbs"
            $output = "$ScriptDir\SilentLaunchPoSh.vbs"                
            $wc = New-Object System.Net.WebClient
            $wc.DownloadFile($url, $output)
            if(-not $(Test-Path "$ScriptDir\SilentLaunchPoSh.vbs")){
                Write-LogError -LogPath $sLogPath -ToScreen -TimeStamp -Message "$ScriptDir\SilentLaunchPoSh.vbs, is missing."
                Write-LogError -LogPath $sLogPath -ToScreen -TimeStamp -Message "The scheduled task will not be created."
                exit
            }
        }
        Write-LogInfo -LogPath $sLogPath -ToScreen -TimeStamp -Message "Creating scheduled task, $TaskName." 
        $ScheduledTask = schtasks /Create /RU $env:USERNAME /SC WEEKLY /D MON,TUE,WED,THU,FRI /ST $ScheduledStart /DU $ScheduledEnd /TN $TaskName /RL HIGHEST /RI 5 /F /TR "hostname"
        $Action = New-ScheduledTaskAction -Execute 'WSCRIPT' -Argument "`"$ScriptDir\SilentLaunchPoSh.vbs`" `"$ScriptPath`"" 
        Set-ScheduledTask -TaskName $TaskName -Action $Action -User $env:USERNAME
        Write-LogInfo -LogPath $sLogPath -ToScreen -TimeStamp -Message $ScheduledTask
    }
}
else{
    $TaskDelete = SCHTASKS /Delete /TN $TaskName /F
}

Write-LogInfo -LogPath $sLogPath -ToScreen -TimeStamp -Message "Starting time frame check."

#Check if ticket has not been entered in $TimeFrame
#Get latest time entries
$condition = "conditions=enteredBy=`'$User`'&orderBy=timeEnd desc"
$LatestTimeEntries = Invoke-RestMethod -Headers $Headers -Uri "https://$($CWServer)/v4_6_release/apis/3.0/time/entries?$condition" -Method GET
[datetime]$LastTimeEntry = $LatestTimeEntries[0].timeEnd
#If no time entry today set to $ScheduledStart
if($LastTimeEntry -lt (Get-Date -Hour 0 -Minute 0 -Second 0)){
    [datetime]$LastTimeEntry = Get-Date $ScheduledStart 
}
#If last time entry was more than $Timeframe minutes old prompt for time input.
if($LastTimeEntry -gt (Get-Date).AddMinutes((($TimeFrame * -1) -2))){
    #Last time entry was within $TimeFrame
    Write-LogInfo -LogPath $sLogPath -ToScreen -TimeStamp -Message "Last time entry was $LastTimeEntry."
    exit
}

#Get CW icon if on machine
if(Get-ChildItem "C:\Program Files*\ConnectWise\PSA.net\ConnectWise.ico"){
    $CWIcon = " Icon='$((Get-ChildItem "C:\Program Files*\ConnectWise\PSA.net\ConnectWise.ico").FullName)'"
}
 Write-LogInfo -LogPath $sLogPath -ToScreen -TimeStamp -Message "Loading XAML"
#Replace with XAML generated by Visual Studio
$inputXAML = @"
<Window x:Class="WpfApplication6.MainWindow"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:local="clr-namespace:WpfApplication6"
        mc:Ignorable="d"
        Title="TimeSucks" Height="425" Width="600" Topmost="True"$CWIcon>
    <Grid>
        <Grid.ColumnDefinitions>
            <ColumnDefinition Width="235*"/>
            <ColumnDefinition Width="15*"/>
            <ColumnDefinition Width="343*"/>
        </Grid.ColumnDefinitions>
        <TextBox x:Name="textBox" Margin="10,72,9.333,43" TextWrapping="Wrap" Grid.ColumnSpan="3" AcceptsReturn="True"/>
        <Label x:Name="StartLabel" Content="Start Time" Height="30" Margin="0,10,156.333,0" VerticalAlignment="Top" RenderTransformOrigin="0.497,-0.795" FontSize="11" Grid.Column="2" HorizontalAlignment="Right" Width="60"/>
        <TextBox x:Name="EndTime" HorizontalAlignment="Right" Height="22" Margin="0,10,73.333,0" TextWrapping="Wrap" VerticalAlignment="Top" Width="78" Grid.Column="2"/>
        <TextBox x:Name="StartTime" Height="22" Margin="0,10,220.333,0" TextWrapping="Wrap" VerticalAlignment="Top" Grid.Column="2" HorizontalAlignment="Right" Width="79"/>
        <Label x:Name="EndLabel" Content="End Time" HorizontalAlignment="Right" Height="30" Margin="0,10,9.333,0" VerticalAlignment="Top" Width="59" RenderTransformOrigin="0.497,-0.795" FontSize="11" Grid.Column="2"/>
        <ComboBox x:Name="comboBox" Height="22" Margin="10,11,82.333,0" VerticalAlignment="Top" IsEnabled="True"/>
        <Button x:Name="Submit" Content="Submit" Margin="0,0,9.333,9" Grid.Column="2" HorizontalAlignment="Right" Width="75" Height="30" VerticalAlignment="Bottom"/>
        <TextBlock x:Name="Status" Margin="10,0,0.333,9" TextWrapping="Wrap" Height="20" VerticalAlignment="Bottom"/>
        <Label x:Name="RecentLabel" Content="Recent" Height="30" Margin="0,37,250.333,0" VerticalAlignment="Top" RenderTransformOrigin="0.497,-0.795" FontSize="11" HorizontalAlignment="Right" Width="43" Grid.Column="2"/>
        <ComboBox x:Name="comboBoxTicket" Grid.ColumnSpan="3" Margin="10,41,298.333,0" Height="21" VerticalAlignment="Top" IsEnabled="True"/>
        <Label x:Name="ChargeCodeLabel" Content="Charge Code" Height="30" Margin="0,11,0.333,0" VerticalAlignment="Top" RenderTransformOrigin="0.497,-0.795" FontSize="11" HorizontalAlignment="Right" Width="74"/>
        <TextBlock x:Name="todaysHours" Grid.Column="2" HorizontalAlignment="Right" Height="19" Margin="0,0,89.333,13" TextWrapping="Wrap" Text="14.05 Hours Today" VerticalAlignment="Bottom" Width="101"/>
        <TextBox x:Name="TicketNumner" Height="22" Margin="0,40,73.333,0" TextWrapping="Wrap" VerticalAlignment="Top" Grid.Column="2" HorizontalAlignment="Right" Width="79"/>
        <Label x:Name="TicketLabel" Content="Ticket #" Height="30" Margin="0,37,10.333,0" VerticalAlignment="Top" RenderTransformOrigin="0.497,-0.795" FontSize="11" Grid.Column="2" HorizontalAlignment="Right" Width="60"/>
        <CheckBox x:Name="TimeWrap" Content="TimeWrap" Grid.Column="2" HorizontalAlignment="Right" Margin="0,43,170.333,0" VerticalAlignment="Top"/>
    </Grid>
</Window>
"@

#region -[Dont mess with this region]-

$inputXAML = $inputXAML -replace 'mc:Ignorable="d"','' -replace "x:N",'N' -replace '^<Win.*', '<Window'
[void][System.Reflection.Assembly]::LoadWithPartialName('presentationframework')
[xml]$XML = $inputXAML

#Read converted XAML
$reader=(New-Object System.Xml.XmlNodeReader $XML) 
try{
    $Form=[Windows.Markup.XamlReader]::Load( $reader )
}
catch [System.Management.Automation.MethodInvocationException] {
    Write-Warning 'Issue with XAML.  Check the syntax for this control...'
    Write-Host $error[0].Exception.Message -ForegroundColor Red
    if ($error[0].Exception.Message -like "*button*"){
        Write-Warning 'Ensure your button in the $inputXAML does NOT have a `Click=ButtonClick` property.'
    }
}
catch{
    Write-Output 'Unable to load Windows.Markup.XamlReader. Double-check syntax and ensure .net is installed.'
}
 
#Store form nodes as PowerShell variables and append with WPF 
$XML.SelectNodes("//*[@Name]") | ForEach-Object{Set-Variable -Name "WPF$($_.Name)" -Value $Form.FindName($_.Name)}

#endregion
 
#region -[Execution]-

#Get admin auth token
$AdminAuth = Get-CWImpersinateAuth -IntegratorUser $IntegratorUser -IntegratorPass $IntegratorPass -MemberID $MemberID

#Get endTime of last time entry and set for startTime
$condition = "conditions=enteredBy = '$User' &orderBy=timeEnd desc"
$LatestTimeEntries = Invoke-RestMethod -Headers $Headers -Uri "https://$CWServer/v4_6_release/apis/3.0/time/entries?$condition" -Method GET
[datetime]$LastTimeEntry = $LatestTimeEntries[0].timeEnd
#If no time entry today set to $ScheduledStart
if($LastTimeEntry -lt (Get-Date).Date){
    [datetime]$LastTimeEntry = Get-Date $ScheduledStart 
}

#Start and stop times
$WPFStartTime.Text = $LastTimeEntry.ToString("t")
$WPFEndTime.Text = (Get-Date).ToString("t")

#Populate Charge Codes 
$ChargeCodes = Get-ChargeCode -Headers $AdminAuth 
foreach($Code in $($ChargeCodes | Select-Object -Property Keys -ExpandProperty Keys)){
  $WPFcomboBox.Items.add($Code) | Out-Null
}

#Populate service tickets
foreach($ID in (($LatestTimeEntries | where {$_.chargeToType -eq 'ServiceTicket'}).chargeToId | Sort-Object | Get-Unique)){
        $ServiceTicket = Invoke-RestMethod -Headers $Headers -Uri "https://$CWServer/v4_6_release/apis/3.0/service/tickets/$ID"
        $CompanyTrimed = $($ServiceTicket.company.identifier.subString(0, [System.Math]::Min(15, ($ServiceTicket.company.identifier).Length)))
        $LastTimeSumary = "$($ServiceTicket.id):$($CompanyTrimed)\$($ServiceTicket.summary)"
        $WPFcomboBoxTicket.Items.add($LastTimeSumary) | Out-Null
}

$WPFStatus.Text = $Quotes | Get-Random

$WPFSubmit.add_click({
    $Valid = 1
    $Test = Test-Time $WPFStartTime.Text
    if(!(Test-Time $WPFStartTime.Text)){
        $Valid = 0
        $WPFStatus.Text = 'Start Time is incorrect.'
    }    
    if(!(Test-Time $WPFEndTime.Text)){
        $Valid = 0
        $WPFStatus.Text = 'End Time is incorrect.'
    }    
    if(!$WPFcomboBox.Text -and !$WPFcomboBoxTicket.Text -and !$WPFTimeWrap.IsChecked){
        $Valid = 0
        $WPFStatus.Text = 'Choose a charge code, service ticket or TimeWrap.'
    }
    if($WPFtextBox.Text.length -lt 20){
        $Valid = 0
        $WPFStatus.Text = 'Enter better notes.'
    }

    if($Valid -eq 1){

        if($WPFcomboBox.Text){
            $Company = Invoke-RestMethod -Headers $Headers -Uri "https://$($CWServer)/v4_6_release/apis/3.0/company/companies?conditions=name='$($CompanyDisplayName)'" -Method Get
            $chargeToId = (($ChargeCodes.GetEnumerator() | Where-Object{$_.Name -eq $WPFcomboBox.Text} | Select Value).Value.GetEnumerator() | Where-Object{$_.Name -eq 'TE_Charge_Code_RecID'}).Value

            $Body = @{
                company = @{
                    id = $Company.id
                    identifier = $Company.identifier
                    name = $Company.name
                    }
                chargeToId = $chargeToId            
                chargeToType = "ChargeCode"
                notes = $($WPFtextBox.Text)
                timeStart = $(Get-Date $WPFStartTime.Text | Convert-Time)
                timeEnd = $(Get-Date $WPFEndTime.Text | Convert-Time)
                billableOption = "NoCharge"
                } | convertto-json
        }
        if($WPFTimeWrap.IsChecked){
            $Company = Invoke-RestMethod -Headers $Headers -Uri "https://$($CWServer)/v4_6_release/apis/3.0/company/companies?conditions=name='$($CompanyDisplayName)'" -Method Get
            $chargeToId = (($ChargeCodes.GetEnumerator() | Where-Object{$_.Name -eq $TimeWrapChargeCode} | Select Value).Value.GetEnumerator() | Where-Object{$_.Name -eq 'TE_Charge_Code_RecID'}).Value

            $Body = @{
                company = @{
                    id = $Company.id
                    identifier = $Company.identifier
                    name = $Company.name
                    }
                chargeToId = $chargeToId            
                chargeToType = "ChargeCode"
                notes = $($WPFtextBox.Text)
                timeStart = $(Get-Date $WPFStartTime.Text | Convert-Time)
                timeEnd = $(Get-Date $WPFEndTime.Text | Convert-Time)
                hoursDeduct = $TodaysTotal 
                billableOption = "NoCharge"
                } | convertto-json
        }

        if($WPFcomboBoxTicket.Text){
            $ServiceTicket = Invoke-RestMethod -Headers $Headers -Uri "https://$CWServer/v4_6_release/apis/3.0/service/tickets/$($WPFcomboBoxTicket.Text.Split(':')[0])"
            
            $Body = @{
                company = $ServiceTicket.Company
                chargeToId = $ServiceTicket.id            
                chargeToType = "ServiceTicket"
                notes = $($WPFtextBox.Text)
                timeStart = $(Get-Date $WPFStartTime.Text | Convert-Time)
                timeEnd = $(Get-Date $WPFEndTime.Text | Convert-Time)
                } | convertto-json
        }
        
        
        $Submit = Invoke-RestMethod -ContentType "application/json" -Method Post -Uri "https://$($CWServer)/v4_6_release/apis/3.0/time/entries" -Headers $Headers -Body $Body
        
        if($Submit){
            if($AutoClose){
                $Form.Close()
                Exit 0
            }
            $WPFStatus.Text = 'Time entry successfully created.'
            $WPFStartTime.Text = Get-Date -f t
            $WPFEndTime.Text = ''
            $WPFtextBox.Text = ''
            $WPFcomboBox.Text = ''                
        }
        else{
            $WPFStatus.Text = 'There was an error creating the time entry.'
            $Error[0]
            $ErrorMessage = @"
$(($Error[0] | ConvertFrom-Json).message)
$(($Error[0] | ConvertFrom-Json).errors.message)
"@    
            $wshell = New-Object -ComObject Wscript.Shell
            $wshell.Popup($ErrorMessage,0,"Error creating Time Entry",0x1010)
        }
    }
})

#Verify only one option is selected
$WPFcomboBox.add_DropDownClosed({
    if($WPFcomboBox.Text){
        $WPFcomboBoxTicket.Text = ''
        $WPFTicketNumner.Text = ''
        $WPFTimeWrap.IsChecked = $false
        $WPFStartTime.Text = $LastTimeEntry.ToString("t")
        $WPFEndTime.Text = (Get-Date).ToString("t")
    }
})
$WPFcomboBoxTicket.add_DropDownClosed({
    if($WPFcomboBoxTicket.Text){
        $WPFcomboBox.Text = ''
        $WPFTicketNumner.Text = '' 
        $WPFTimeWrap.IsChecked = $false
        $WPFStartTime.Text = $LastTimeEntry.ToString("t")
        $WPFEndTime.Text = (Get-Date).ToString("t")   
    }
})
$WPFTicketNumner.add_LostFocus({
    if($WPFTicketNumner.Text){
        $WPFcomboBox.Text = ''
        $WPFcomboBoxTicket.Text = ''
        $WPFTimeWrap.IsChecked = $false
        $WPFStartTime.Text = $LastTimeEntry.ToString("t")
        $WPFEndTime.Text = (Get-Date).ToString("t")   
    }
})
$WPFTimeWrap.add_Click({
    if($WPFTimeWrap.IsChecked){
        $WPFTicketNumner.Text = ''
        $WPFcomboBox.Text = ''
        $WPFcomboBoxTicket.Text = '' 
        $WPFStartTime.Text = (Get-Date $ScheduledStart).ToString("t")
        $WPFEndTime.Text = (Get-Date $ScheduledEnd).ToString("t")     
    }
})

$TodaysTotal = ((($LatestTimeEntries | Where-Object{[DateTime]$_.timeStart -gt (Get-Date).Date}).actualHours) | Measure-Object -Sum).Sum
$WPFtodaysHours.text = "$TodaysTotal hours today"

#endregion
Write-LogInfo -LogPath $sLogPath -ToScreen -TimeStamp -Message "Launching form."
#Shows the form
$Form.ShowDialog() | out-null